// TODO
/*

	 - ne peu t pas inviter des gens si pas co 
	 - email personne inscrit non envoyer 
	 - verb ask au lieu de invite
	- 




*/
var inviteObj = {
	container : "",
	init : function(initParams){
		inviteObj.container = initParams.container;
	},
	formInvite : function(callback){
		mylog.log("finder formInvite ", inviteObj.container, callback, inviteObj);


		$(inviteObj.container+' #btnInviteNew').off().on("click", function(e){
			mylog.log("finder btnInviteNew ", inviteObj);
			var formInvite = $(inviteObj.container);
			mylog.log("finder form ", formInvite, inviteObj);
			formInvite.submit(function(e){ mylog.log("finder submit ", e); e.preventDefault() });
			var errorHandler = $('.errorHandler', formInvite);
			mylog.log("finder errorHandler ", errorHandler);
			formInvite.validate({
				rules : {
					inviteEmail : {
						minlength : 2,
						required : true,
						email: true
					},
					inviteName : {
						minlength : 2,
						required : true,
					},
					inviteText : {
						maxlength : 500,
					}
				},
				submitHandler : function(formInvite) {

					//alert("submitHandler");
					mylog.log("finder submitHandler form", formInvite, inviteObj);
					var mail = $(inviteObj.container+' #inviteEmail').val();
					var msg = $(inviteObj.container+' #inviteText').val();
					var name = $(inviteObj.container+' #inviteName').val();
					errorHandler.hide();
					if(typeof callback != "undefined" && callback != null){

						var data = {
							id : inviteObj.keyUniqueByMail(mail),
							mail : mail,
							msg : msg,
							name : name,
							type : "citoyens"
						}
						$(inviteObj.container+' #inviteEmail').val("");
						$(inviteObj.container+' #inviteText').val("");
						$(inviteObj.container+' #inviteName').val("");
						callback(data);
					}else{
						

						if(typeof listInvite.invites[mail] == "undefined"){
							var keyUnique = keyUniqueByMail(mail);
							listInvite.invites[keyUniqueByMail(mail)] = {
								name : name,
								mail : mail,
								msg : msg
							} ;

							if(parentType != "citoyens")
								listInvite.invites[keyUnique].isAdmin = "";

							$(inviteObj.container+' #inviteEmail').val("");
							$(inviteObj.container+' #inviteText').val("");
							$(inviteObj.container+' #inviteName').val("");
							$(inviteObj.container+" #form-invite").hide();
						} else {
							toastr.error(tradDynForm.alreadyInTheList);
						}
						showElementInvite(listInvite, true);
						bindRemove();
					}
				},
				invalidHandler: function(event, validator) {
					//alert("invalidHandler");
					mylog.log("finder invalidHandler", event, validator)
					var errors = validator.numberOfInvalids();
					if (errors) {
						var message = errors == 1
						? 'You missed 1 field. It has been highlighted'
						: 'You missed ' + errors + ' fields. They have been highlighted';
						mylog.log("finder message", message);
						$("div.error span").html(message);
						$("div.error").show();
					} else {
						$("div.error").hide();
					}
				}

			});
		});
	},
	keyUniqueByMail(mail) {
		var keyUnique = "";
		for (var i=0; i < mail.length; i++) {
			keyUnique += mail.charCodeAt(i);
		}
		return keyUnique ;
	}
};

//// OLD 
// var inviteObj = {
// 	rolesListCustom : null,
// 	parentId : null,
// 	parentLinks : null,
// 	rolesListCustom : null,
// 	isElementAdmin : null,
// 	listInvite : { 
// 			citoyens : {},
// 			organizations : {},
// 			invites : {},
// 	},
// 	init : function (initParams) {
// 		inviteObj.parentType = ( (typeof initParams.parentType != "undefined") ? initParams.parentType : null ) ;
// 		inviteObj.parentId = ( (typeof initParams.parentId != "undefined") ? initParams.parentId : null ) ;
// 		inviteObj.parentLinks = ( (typeof initParams.parentLinks != "undefined") ? initParams.parentLinks : null ) ;
// 		inviteObj.rolesListCustom = ( (typeof initParams.rolesListCustom != "undefined") ? initParams.rolesListCustom : null ) ;
// 		inviteObj.isElementAdmin= ( (typeof initParams.isElementAdmin != "undefined") ? initParams.isElementAdmin : null ) ;


// 		$('#btnInviteNew').click(function(e){
// 			var form = $('#form-invite');
// 			var loginBtn = null;
// 			form.submit(function(e){ e.preventDefault() });

// 			var errorHandler = $('.errorHandler', form);

// 			form.validate({
// 				rules : {
// 					inviteEmail : {
// 						minlength : 2,
// 						required : true,
// 						email: true
// 					},
// 					inviteName : {
// 						minlength : 2,
// 						required : true,
// 					},
// 					inviteText : {
// 						maxlength : 500,
// 					}
// 				},
// 				submitHandler : function(form) {
// 					errorHandler.hide();
// 					var mail = $('#form-invite #email').val();
// 					var msg = "";
// 					var name = "tetst";

// 					if(typeof inviteObj.listInvite.invites[mail] == "undefined"){
// 						var keyUnique = inviteObj.keyUniqueByMail(mail);
// 						inviteObj.listInvite.invites[keyUnique] = {
// 							name : name,
// 							mail : mail,
// 							msg : msg
// 						} ;

// 						inviteObj.multiconnect();
// 					} else {
// 						toastr.error(tradDynForm.alreadyInTheList);
// 					}
// 				} 
// 			});
// 		});

// 		$('#modal-invite #btnValider').click(function(e){
			
// 		});
// 	},
// 	keyUniqueByMail : function(mail) {
// 		mylog.log("keyUniqueByMail", mail);
// 		var keyUnique = "";
// 		for (var i=0; i < mail.length; i++) {
// 			keyUnique += mail.charCodeAt(i);
// 		}
// 		return keyUnique ;
// 	},
// 	multiconnect : function(){
// 		mylog.log("multiconnect", Object.keys(inviteObj.listInvite.organizations).length, Object.keys(inviteObj.listInvite.citoyens).length);
// 		if( Object.keys(inviteObj.listInvite.organizations).length > 0 || 
// 			Object.keys(inviteObj.listInvite.citoyens).length > 0 || 
// 			Object.keys(inviteObj.listInvite.invites).length > 0 ) {
// 			mylog.log("#modal-invite #btnValider");

// 			var params = {
// 				parentId : inviteObj.parentId,
// 				parentType : inviteObj.parentType,
// 				listInvite : inviteObj.listInvite
// 			};

// 			$.ajax({
// 				type: "POST",
// 				url: baseUrl+'/co2/link/multiconnect',
// 				data: params,
// 				dataType: "json",
// 				success: function(data){

// 					mylog.log("link/multiconnect success", data);
// 					var nbInvites = data.length;
// 					var str = "";
// 					if(typeof data.citoyens != "undefined"){
						
// 					}

// 					if(typeof data.invites != "undefined"){
						
// 					}
// 					toastr.success(trad["processing"]);						
// 			 	}
// 			});
			
// 		}
// 	}
// }