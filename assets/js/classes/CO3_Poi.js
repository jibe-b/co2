var CO3_Poi = class extends CO3_Element{
  
  constructor(pRequeteAjax,pPHDB_Obj){
    super(pRequeteAjax,pPHDB_Obj);  
    this.COLLECTION = "poi";
    this.CONTROLLER = "poi";
    this.MODULE = "poi";
    this.ICON = "fa-map-marker";  
  }

  PrepareRequeteAjax(pRequeteAjax){
    pRequeteAjax=super.PrepareRequeteAjax(pRequeteAjax);
    pRequeteAjax["where"]["type"]="poi";
    return pRequeteAjax;
  }

}

CO3_Poi.name = "CO3_Poi";