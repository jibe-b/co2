<?php 
    $cssAnsScriptFilesModule = array(
    '/plugins/jquery-simplePagination/jquery.simplePagination.js',
    '/plugins/jquery-simplePagination/simplePagination.css',
    '/plugins/facemotion/faceMocion.css',
    '/plugins/facemotion/faceMocion.js',
    );
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->getRequest()->getBaseUrl(true));
    $cssAnsScriptFilesModule = array(
    '/assets/css/default/responsive-calendar.css',
    '/assets/css/default/search.css',
    '/assets/js/comments.js'
    );
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->theme->baseUrl);

    $cssAnsScriptFilesTheme = array(
        // SHOWDOWN
        '/plugins/showdown/showdown.min.js',
        //MARKDOWN
        '/plugins/to-markdown/to-markdown.js',        
    );
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);

    $cssAnsScriptFilesModule = array(
    '/js/default/responsive-calendar.js',
    '/js/default/search.js',
    '/js/news/index.js',
    );
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->getParentAssetsUrl());
    // HtmlHelper::registerCssAndScriptsFiles( array('/css/default/directory.css', ) , 
      //                                    Yii::app()->theme->baseUrl. '/assets');


    $layoutPath = 'webroot.themes.'.Yii::app()->theme->name.'.views.layouts.';

    $maxImg = 5;
    $page = (@$page && !empty($page)) ? $page : "search";
    if(!@$type){  $type = "all"; }

    if(@$type=="events")    { $page = "agenda"; $maxImg = 7; }
    if(@$type=="classifieds"){ $page = "annonces"; $maxImg = 1; }
    if(@$type=="ressources") { $page = "ressources"; }
    if(@$type=="proposals") { $page = "dda";}

    $filliaireCategories = CO2::getContextList("filliaireCategories");
    $directoryParams=Yii::app()->session["paramsConfig"]["directory"];
    if(@Yii::app()->session["paramsConfig"]["pages"]["#".$page]["directory"])
        $directoryParams=Yii::app()->session["paramsConfig"]["pages"]["#".$page]["directory"];
    //header + menu
    $this->renderPartial($layoutPath.'header', 
                            array(  "layoutPath"=>$layoutPath ,
                                    "page" => $page,
                                    "type" => @$type)
                        ); 

?>

<style>

    #dropdown_search{
        margin-top:0px;
    }

    .container{
        padding-bottom:0px !important;
    }
    .simple-pagination{
        padding: 5px 0px;
        border-bottom: 1px solid #e9e9e9;
      /*  font-family: "Montserrat", "Helvetica Neue", Helvetica, Arial, sans-serif;*/
    }

    .simple-pagination li a, .simple-pagination li span {
        border: none;
        box-shadow: none !important;
        background: none !important;
        color: #707478 !important;
        font-size: 13px !important;
        font-weight: 500;
    }
    .simple-pagination li.active span{
        color: #d9534f !important;
        font-size: 24px !important; 
    }

    .simple-pagination li a.next,
    .simple-pagination li a.prev{
        color:#223f5c !important;
    }
    .btn-tags-unik{
        margin-top: 5px;
        border-radius:3px;
        color: white;
    }
    .btn-tags-unik:hover{
        color:white;
        text-decoration: none;
    }
    .btn-tags-unik.active{
        background-color: white !important;
        color: red !important ;
    }
</style>


<div class="col-md-12 col-sm-12 col-xs-12 bg-white no-padding shadow pageContent" 
     id="content-social" style="min-height:700px;">

	<div class="col-md-12 col-sm-12 col-xs-12 no-padding" id="page">
        <?php if(isset(Yii::app()->session["costum"]) && isset(Yii::app()->session["costum"]["htmlConstruct"]) && isset(Yii::app()->session["costum"]["htmlConstruct"]["directory"]) && isset(Yii::app()->session["costum"]["htmlConstruct"]["directory"]["filters"]) && isset(Yii::app()->session["costum"]["htmlConstruct"]["directory"]["filters"]["tagsList"]) && isset(Yii::app()->session["costum"]["tags"])){ ?> 
            <div class="col-md-10 col-sm-10 col-xs-12 text-left col-sm-offset-1 col-md-offset-1 text-center margin-top-20">
            <a class='btn btn-link bg-purple elipsis btn-tags-unik'
                    data-k='' href='javascript:;'>
                    <?php echo Yii::t("common","See all") ?>
            </a>
            <?php 
            foreach(Yii::app()->session["costum"]["tags"] as $v){ ?>
                <a class='btn btn-link bg-purple elipsis btn-tags-unik'
                    data-k='<?php echo $v ?>' href='javascript:;'>
                    <?php echo $v ?>
                </a>
        <?php } ?>
        </div> 
        <?php } ?>
        <?php if(!empty($directoryParams["header"])){ ?>
        <div class="headerSearchContainer no-padding col-md-10 col-sm-10 col-xs-12 text-left col-sm-offset-1 col-md-offset-1">
        </div>
        <?php } ?>
        <?php if(@$type=="events"){ ?>
            <div class="col-xs-12 no-padding calendar margin-bottom-20"></div>
            <div class="responsive-calendar-init hidden"> 
              <div class="responsive-calendar light col-md-12 no-padding">   
                  <div class="day-headers">
                    <div class="day header"><?php echo Yii::t("translate","Mon") ?></div>
                    <div class="day header"><?php echo Yii::t("translate","Tue") ?></div>
                    <div class="day header"><?php echo Yii::t("translate","Wed") ?></div>
                    <div class="day header"><?php echo Yii::t("translate","Thu") ?></div>
                    <div class="day header"><?php echo Yii::t("translate","Fri") ?></div>
                    <div class="day header"><?php echo Yii::t("translate","Sat") ?></div>
                    <div class="day header"><?php echo Yii::t("translate","Sun") ?></div>
                  </div>
                  <div class="days" data-group="days"></div>   
                  <div class="controls">
                      <a id="btn-month-before" class="text-white" data-go="prev"><div class="btn"><i class="fa fa-arrow-left"></i></div></a>
                      <h4 class="text-white"><span data-head-month></span> <span data-head-year></span></h4>
                      <a id="btn-month-next" class="text-white" data-go="next"><div class="btn"><i class="fa fa-arrow-right"></i></div></a>
                  </div>
              </div>
            </div>
        <?php } ?>
        <div class="col-md-10 col-sm-10 col-sm-offset-1 col-md-offset-1 col-xs-12 bodySearchContainer margin-top-10 <?php echo $page ?>">
            <div class="no-padding col-xs-12" id="dropdown_search">
              <div class='col-md-12 col-sm-12 text-center search-loader text-dark'>
                  <i class='fa fa-spin fa-circle-o-notch'></i> <?php echo Yii::t("common","Currently researching") ?> ...
              </div>
            </div>
            <div class="no-padding col-xs-12 text-left footerSearchContainer"></div>   
        </div>
    </div>
<?php $this->renderPartial($layoutPath.'modals.'.Yii::app()->params["CO2DomainName"].'.pageCreate', array()); ?>
<?php $this->renderPartial($layoutPath.'footer', array(  "page" => $page)); ?>


<script type="text/javascript" >

var type = "<?php echo @$type ? $type : 'all'; ?>";
var typeInit = "<?php echo @$type ? $type : 'all'; ?>";
var appParamsKey=(location.hash.indexOf("?") >= 0) ? location.hash.split("?")[0] : location.hash;
var pageCount=false;
searchObject.count=true;
searchObject.initType=typeInit;
<?php if(@$type=="events"){ ?>
  var STARTDATE = new Date();
  var ENDDATE = new Date();
  var startWinDATE = new Date();
  var agendaWinMonth = 0;
<?php } ?>
var scrollEnd = false;
if(searchObject.initType=="events") var categoriesFilters=<?php echo json_encode(Event::$types) ?>;
if(searchObject.initType=="all"){
  var categoriesFilters={
    "persons" : { "key": "persons", "icon":"user", "label":"people","color":"yellow"}, 
    "NGO" : { "key": "NGO", "icon":"group", "label":"NGOs","color":"green-k"}, 
    "LocalBusiness" : { "key": "LocalBusiness", "icon":"industry", "label":"LocalBusiness","color":"azure"}, 
    "Group" : { "key": "Group", "icon":"circle-o", "label":"Groups","color":"turq"}, 
    "GovernmentOrganization" : { "key": "GovernmentOrganization", "icon":"university", "label":"services publics","color":"red"},
    "projects" : { "key": "projects", "icon":"lightbulb-o", "label":"projects","color":"purple"}, 
    "events" : { "key": "events", "icon":"calendar", "label":"events","color":"orange"}, 
    "poi" : { "key": "poi", "icon":"map-marker", "label":"points of interest","color":"green-poi"}, 
    //"place" : { "key": "place", "icon":"map-marker", "label":"points of interest","color":"brown"},
    //"places" : { "key": "places", "icon":"map-marker", "label":"Places","color":"brown"}, 
    "classifieds" : { "key": "classified", "icon":"bullhorn", "label":"classifieds","color":"azure"}, 
     
    //"ressources" : { "key": "ressources", "icon":"cubes", "label":"Ressource","color":"vine"} 
    //"services" : { "key": "services", "icon":"sun-o", "label":"services","color":"orange"}, "circuits" : { "key": "circuits", "icon":"ravelry", "label":"circuits","color":"orange"},
  };
}

var filliaireCategories = <?php echo json_encode($filliaireCategories); ?>;
var currentKFormType = "";

jQuery(document).ready(function() {
    initKInterface();
    initCountType();
    loadingData = false; 
    searchInterface.init(type);
    startSearch(searchObject.indexMin, null, searchCallback);

    if(page=="agenda")
        calculateAgendaWindow(0);
    $(".tooltips").tooltip();
});


/* -------------------------
AGENDA
----------------------------- */

<?php if(@$type == "events"){ ?>

var calendarInit = false;
function showResultInCalendar(mapElements){
    var events = new Array();
    var fstDate = "";
    console.log("data mapElements", mapElements);
    $.each(mapElements, function(key, thisEvent){
    
        var startDate = exists(thisEvent["startDateTime"]) ? thisEvent["startDateTime"].substr(0, 10) : "";
        var endDate = exists(thisEvent["endDateTime"]) ? thisEvent["endDateTime"].substr(0, 10) : "";

        var cp = "";
        var loc = "";
        if(thisEvent["address"] != null){
            var cp = exists(thisEvent["address"]["postalCode"]) ? thisEvent["address"]["postalCode"] : "" ;
            var loc = exists(thisEvent["address"]["addressLocality"]) ? thisEvent["address"]["addressLocality"] : "";
        }
        var position = cp + " " + loc;

        var name = exists(thisEvent["name"]) ? thisEvent["name"] : "";
        var thumb_url = notEmpty(thisEvent["profilThumbImageUrl"]) ? baseUrl+thisEvent["profilThumbImageUrl"] : "";
        
        if(typeof events[startDate] == "undefined") events[startDate] = new Array();
        events[startDate].push({  "id" : (typeof thisEvent["_id"] != "undefined") ? thisEvent["_id"]["$id"] : thisEvent["id"]  ,
                                  "thumb_url" : thumb_url, 
                                  "startDate": startDate,
                                  "endDate": endDate,
                                  "name" : name,
                                  "position" : position });
    });

    if(calendarInit == true) {
        $(".calendar").html("");
    }

    $(".calendar").html($(".responsive-calendar-init").html());
    var aujourdhui =(searchObject.startDate != "undefined") ? new Date(searchObject.startDate*1000) : startWinDATE; //new Date();
    var  month = (aujourdhui.getMonth()+1).toString();
    if(aujourdhui.getMonth() < 10) month = "0" + month;
    var date = aujourdhui.getFullYear().toString() + "-" + month;

    $(".responsive-calendar").responsiveCalendar({
          time: date,
          events: events
        });

    $(".responsive-calendar").show();
    calendarInit = true;
}

<?php } ?>

/* -------------------------
END AGENDA
----------------------------- */

</script>