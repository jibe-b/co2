<?php //var_dump($element); ?>
<div id="answersList" class="bg-white shadow2 col-xs-12 no-padding">

</div>
<script type="text/javascript">
	var elementAnswer=<?php echo json_encode($element); ?>; 
	var type=<?php echo json_encode($type); ?>;
	var eltObj=typeObj.get(type);
	jQuery(document).ready(function() {	
		if(jsonHelper.notNull("eltObj.dynFormCostum.beforeBuild.properties")){
			str="";
			$.each(eltObj.dynFormCostum.beforeBuild.properties, function(e,v){
				str+="<div class='col-xs-12 padding-10'>";
					if(typeof v.label != "undefined"){
						str+="<h5 class='answersLabel col-xs-12 text-dark'>"+
							"<i class='fa fa-caret-down'></i> "+
							v.label+
							"</h5>";
					}
					if(typeof v.inputType != "undefined" 
						&& typeof v.docType!="undefined" 
						&& v.inputType=="uploader" 
						&& v.docType=="file" 
						&& typeof elementAnswer.files != "undefined")
						str+=renderFile(elementAnswer.files);
					else{
						if(typeof elementAnswer[e] != "undefined"){
							if(typeof elementAnswer[e] == "object"){
								if(e=="scope")
									str+=getScopeName(elementAnswer[e]);
							}else{
								markdown=(typeof v.markdown != "undefined") ? "markdown" : "";
								str+="<span class='answersValue col-xs-12 text-justify "+markdown+"'>"+elementAnswer[e]+"</span>";
							}
						}else{
							str+="<span class='answersValue col-xs-12 italic'>À compléter</span>";
						}
					}
				str+="</div>";
			});
			$("#answersList").html(str);
			$(".answersValue.markdown").each(function(){
				descHtml = dataHelper.markdownToHtml($(this).html());
		  		$(this).html(descHtml);
			});
		}
	});
	function getScopeName(scopes){
		scopeStr="";
		$.each(scopes, function(k, scope){
			scopeStr+="<span class='col-xs-12 uppercase bold' style='font-size:20px;'><i class='fa fa-crosshairs text-red'></i> ";
				if(typeof scope.name != "undefined")
					scopeStr+=scope.name;
			scopeStr+="</span>";
		});
		return scopeStr;
	}
	function renderFile(files){ 
		fileStr="";
		$.each(files, function(k,file){
			fileStr+="<div class='col-xs-12 padding-5 shadow2 margin-top-5 margin-bottom-5'>"+
				"<a href='"+file.docPath+"' target='_blank' class='link-files'>"+
					"<i class='fa fa-file-pdf-o text-red'></i> "+file.name+
				"</a>"+
			"</div>";
		});
		return fileStr;
	}
</script>