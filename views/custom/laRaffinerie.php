<?php 

/**
/* TEMPLATE LA RAFFINERIE
/*
/* */


// ADDITIONAL FILES

	// THEME

	$cssAndScriptFilesTheme = array(
		// SHOWDOWN
		'/plugins/showdown/showdown.min.js',
		// MARKDOWN
		'/plugins/to-markdown/to-markdown.js',
		'/plugins/fullcalendar/fullcalendar/fullcalendar.min.js',
		'/plugins/fullcalendar/fullcalendar/fullcalendar.css', 
		'/plugins/fullcalendar/fullcalendar/locale/'.Yii::app()->language.'.js',
	);
	HtmlHelper::registerCssAndScriptsFiles($cssAndScriptFilesTheme, Yii::app()->request->baseUrl);


	// MODULES

	$cssAndScriptFilesModule = array('/js/default/calendar.js');
	HtmlHelper::registerCssAndScriptsFiles($cssAndScriptFilesModule, $this->module->assetsUrl);
	HtmlHelper::registerCssAndScriptsFiles( array('/css/calendar.css'), Yii::app()->theme->baseUrl. '/assets');


	// CUSTOM CSS
	
	HtmlHelper::registerCssAndScriptsFiles(array('/css/laRaffinerie.css'), $this->module->assetsUrl);


// CUSTOM VARS

	// ELEMENT

	// $type = Organization::COLLECTION;
	// $elt = PHDB::findOne($type,array("slug" => "laRaffinerie"));
	// $idElt = (String)$elt["_id"] ;
	// $linksElt = Element::getAllLinks($elt["links"], $type, $idElt);
	// $icon = Element::getFaIcon($type) ? Element::getFaIcon($type) : "";
	// $iconColor = Element::getColorIcon($type) ? Element::getColorIcon($type) : "";
	$edit = false;


	// COLORS

	$bgColor1 = "#01809b";
	$bgColorHover1 = "#01607b";

	$projectColorDefault = "rgba(239,42,0, 1)";
	$projectColor = [
		'rgba(140,87,79, 1)',
		'rgba(218,107,15, 1)',
		'rgba(100,40,114, 1)',
	]
?>

<!-- CUSTOM STYLE -->

	<style>
		.customBtnTrigger{
			background-color: <?php echo $bgColor1 ?>;
	  }
	  .customBtnTrigger:hover{
			background-color: <?php echo $bgColorHover1 ?>;
	  }
	  .projectNavFirstLvl > li a:hover::after{
			background-color: <?php echo $projectColorDefault ?>;
		}
		.projectNavSecondLvl .projectNavThirdLvl a:hover::after{
			background-color: <?php echo $projectColorDefault ?>;
		}
		.projectNavSecondLvl >  li a:hover::after{
			background-color: <?php echo str_replace('1)','.82)',$projectColorDefault) ?>;
		}
		<?php 
			$i=1;
			foreach ($projectColor as $key => $value) {
				echo "
					.projectNavSecondLvl .projectNavThirdLvl:nth-of-type(".$i.") a:hover::after{
						background-color: ".str_replace('1)','.82)',$value).";
					}
					.projectNavSecondLvl >  li:nth-of-type(".$i.") a:hover::after{
						background-color: ".$value.";
					}
				";
				$i++;
			}
		?>
	</style>


<!-- HTML START -->

	<div class="project">
		<a href="" class="projectNavTriggerMobile"><i class="fa fa-bars tooltips"></i></a>
		<div class="projectNav">
			<div class="projectNavContent">
				<button class="toggleAllProjects">Ouvrir / Fermer sous-projets</button>
				<?php

					function getCountMembers($el){
						return count(@$el['links']['members']);
					}
					function getCountEvents($el){
						return count(@$el['links']['events']);
					}
					function getCountProjects($el){
						return count(@$el['links']['projects']);
					}
					function getAllEvents($el){
						$arr = [];
						if(@$el['links']['events']){
							foreach ($el['links']['events'] as $key => $value) {
								array_push($arr, $key);
							}
						}
						return $arr;
					}

					$allProjectsCount = 0;
					$allMembersCount = 0;
					$projectList = [];
					$eventsList = [];
					$newsList = [];
					$docsList = [];
					$toolsList = [];
					$allLinksList = [];
					$colorCount = 0;

					// LEVEL 1
					$type = Project::COLLECTION;
					$elt = PHDB::findOne($type,array("slug" => "laRaffinerie1"));
					$idElt = (String)$elt["_id"] ;
					$linksElt = Element::getAllLinks($elt["links"], $type, $idElt);
					$allLinksList = array_merge($allLinksList,$linksElt);
					array_push($projectList, $idElt);
					
					$hasRC = ( @$elt["hasRC"] ) ? "true" : "false" ;
	  			$canEdit = "false";
	  			$loadChat = StringHelper::strip_quotes($elt["name"]);

					if(!empty($elt) && !empty($elt["links"])){

						echo "
							<ul class='projectNavFirstLvl'>
								<li>
									<a href='#' class='linkMenu linkOrganization' 
										data-key='".$idElt."' 
										data-col='".$type."'
										data-countmembers='".getCountMembers($elt)."'
										data-countevents='".getCountEvents($elt)."'
										data-countprojects='".getCountProjects($elt)."'
										data-color='".$projectColorDefault."';
									>".$elt["name"]."</a>
								</li>";
						
						$eventsList = array_merge($eventsList, getAllEvents($elt));
						array_push($projectList, $idElt);

						// LEVEL 2

						if(!empty($elt["links"]["projects"])){

							echo "<ul class='projectNavSecondLvl'>";	

							foreach ($elt["links"]["projects"] as $idElt2 => $valElt2) {

								$elt2 = Element::getElementById($idElt2, "projects");
								$linksElt2 = Element::getAllLinks($elt2["links"], $type, $idElt);
								$allLinksList = array_merge($allLinksList,$linksElt2);
								array_push($projectList, $idElt2);
								$allProjectsCount++;
								$eventsList = array_merge($eventsList, getAllEvents($elt2));

								echo "
									<li>
										<i class='toggleProjects fa fa-plus' aria-hidden='true'></i>
										<a href='#".$elt2["slug"]."' class='linkMenu projectBgColorAfterHover projectBgColorAfterActive' 
											data-key='".$idElt2."'
											data-slug='".$elt2["slug"]."'
											data-col='".Project::CONTROLLER."' 
											data-img='".Yii::app()->createUrl(@$elt2['profilMediumImageUrl'])."'
											data-countmembers='".getCountMembers($elt2)."'
											data-countevents='".getCountEvents($elt2)."'
											data-countprojects='".getCountProjects($elt2)."'
											data-color='".$projectColor[$colorCount]."'
										>".$elt2["name"]."</a></li>";

								// LEVEL 3

								if(!empty($elt2["links"]["projects"])){

									echo "<ul class='projectNavThirdLvl'>";

									foreach ($elt2["links"]["projects"] as $idElt3 => $valElt3) {

										$elt3 = Element::getElementById($idElt3, "projects");
										$linksElt3 = Element::getAllLinks($elt3["links"], $type, $idElt);
										$allLinksList = array_merge($allLinksList,$linksElt3);
										array_push($projectList, $idElt3);
										$allProjectsCount++;
										$eventsList = array_merge($eventsList, getAllEvents($elt3));

										echo "
											<li>
												<a href='#".$elt3["slug"]."' class='linkMenu' 
													data-key='".$idElt3."' 
													data-slug='".$elt3["slug"]."'
													data-col='".Project::CONTROLLER."' 
													data-img='".Yii::app()->createUrl(@$elt3['profilMediumImageUrl'])."'
													data-countmembers='".getCountMembers($elt3)."'
													data-countevents='".getCountEvents($elt3)."'
													data-countprojects='".getCountProjects($elt3)."'
													data-color='".str_replace('1)','.82)',$projectColor[$colorCount])."'
												>".$elt3["name"]."</a></li>";
									}
									echo "</ul>"; $colorCount++;
								}
							}
							echo "</ul>"; 
						}
						echo "</ul>";
					}
				?>
			</div>
		</div>

		<?php 

			function getProfilBannerUrl($el){
				if ( !@$el["profilBannerUrl"] || (@$el["profilBannerUrl"] && empty($el["profilBannerUrl"]))){
					return Yii::app()->theme->baseUrl. '/assets/img/background-onepage/connexion-lines.jpg';
				}
				else{
					return Yii::app()->createUrl($el["profilBannerUrl"]);
				}
			}
			function getProfileThumbUrl($el){
				if ( !@$el["profilMediumImageUrl"] || (@$el["profilMediumImageUrl"] && empty($el["profilMediumImageUrl"]))){
					return Yii::app()->theme->baseUrl.'/assets/img/background-onepage/connexion-lines.jpg';
				}
				else{
					return Yii::app()->createUrl($el["profilMediumImageUrl"]);
				}
			}
			function getDescription($el){
				if ( !@$el["description"] || (@$el["description"] && empty($el["description"]))){
					return false;
				}
				else{
					return $el["description"];
				}
			}
			function getShortDescription($el){
				if ( !@$el["shortDescription"] || (@$el["shortDescription"] && empty($el["shortDescription"]))){
					return false;
				}
				else{
					return $el["shortDescription"];
				}
			}

			$bannerImgUrl 		= getProfilBannerUrl($elt);
			$thumbImgUrl 			= getProfileThumbUrl($elt);
			$shortDescription = getShortDescription($elt);


			$nbInscrits = (!empty($elt['links']['members']) ? count(@$elt['links']['members']) : 0);
				
		?>

		<div class="projectMask">
			<div class="projectMaskContent">
				<div class="projectMaskCell">
					<img src="https://loading.io/spinners/dual-ring/lg.dual-ring-loader.gif" alt="">
				</div>
			</div>
		</div>
		<div class="projectWrapper">
			<div class="projectHeader">
				<h1 class="projectName"><?php echo $elt["name"]; ?></h1>
				<p class="projectShortDescription"><?php echo $shortDescription; ?></p>
				<div class="projectBanner" style="background-image: url(<?php //echo $bannerImgUrl; ?>)"></div>
				<div class="projectThumb"  style="background-image: url(<?php //echo $thumbImgUrl; ?>)"></div>
				<div class="projectHeaderOptions">
					<div class="row">
						<div class="col-xs-12 col-md-4 col-md-offset-2 col-md-push-6 projectHeaderOptionsCont1">
							<div class="col-xs-6">
								<a href="https://chat.communecter.org/channel/laRaffinerie" target="_blank" class="customBtnFull projectBgColor projectBgColorHover"><?php echo Yii::t("cooperation", "Chat");?></a>
							</div>
							<div class="col-xs-6">
								<a href="javascript:;" class="customBtnFull customTabTrigger projectBgColor projectBgColorHover">S'inscrire</a>
							</div>
						</div>
						<div class="col-xs-12 col-md-6 col-md-pull-6 projectHeaderOptionsCont2">
							<div class="col-xs-4 customTabTriggerDescr"><a href="#projectDescription" class="customTabTrigger customBtnLine">À propos</a></div>
							<div class="col-xs-4 customTabTriggerProj" ><a href="#projectChildren" class="customTabTrigger customBtnLine">Projets</a></div>
							<div class="col-xs-4 customTabTriggerContacts"><a href="#projectContacts" class="customTabTrigger customBtnLine">Contacts</a></div>
						</div>
						<!-- <div class="col-sm-3"><span class="projectNbMembers"></span> Inscrits</div>
						<div class="col-sm-3"><span class="projectNbEvents"></span> Évenements</div>
						<div class="col-sm-3"><span class="projectNbProjects"><?php echo $allProjectsCount ?></span> Projets</div> -->
					</div>
				</div>
			</div>

			<?php 
				//var_dump($linksElt);
			?>
			
			<?php $description = getDescription($elt);?>
			<div id="descriptionMarkdown" class="hidden"><?php echo (empty($description) ? "" : $description ); ?></div>
			<div id="projectDescription" class="customBlock customTab">
				<button class="closeCustomTab">X</button>
				<div id="descriptionAbout"></div>
				<button class="customBtnTrigger">Lire la suite <i class="fa fa-angle-down"></i></button>
			</div>

			<div id="projectChildren" class="customBlock customTab">
				<button class="closeCustomTab">X</button>
				<!-- <h3>Les Projets de : <b class="projectNameParent"></b></h3> -->
				<div class="row"></div>
			</div>

			<div id="projectContacts" class="customBlock customTab">
				<button class="closeCustomTab">X</button>
				<div class="row"></div>
			</div>

			<div class="projectInfos customBlock">

				<div class="projectInfosHeader">
					<ul class="row nav nav-tabs"">
						<li class="nav-tab col-sm-3 active projectBgColorActive projectBgColorHover"><a " data-toggle="tab" href="#journal">Journal</a></li>
						<li class="nav-tab col-sm-3 projectBgColorActive projectBgColorHover"><a data-toggle="tab" href="#agenda">Agenda</a></li>
						<li class="nav-tab col-sm-3 projectBgColorActive projectBgColorHover"><a data-toggle="tab" href="#documents">Fichiers</a></li>
						<li class="nav-tab col-sm-3 projectBgColorActive projectBgColorHover"><a data-toggle="tab" href="#outils">Outils</a></li>
					</div>
				</ul>
				
				<style>
					.customBlockImg{
						padding-bottom: 50%;
						background-position: center;
						background-size: cover;
					}
				</style>

				<div class="projectInfosContent tab-content">
					<div id="journal" class="tab-pane active row">Journal</div>
					<div id="agenda" class="tab-pane row">
						<!-- <div class='col-xs-12 margin-bottom-10'>
							<a href='javascript:;' id='showHideCalendar' class='text-azure' data-hidden='0'><i class='fa fa-caret-up'></i> Hide calendar</a>
						</div> -->
						<div id='profil-content-calendar' class='col-xs-12 margin-bottom-20'></div>
						<?php

							$event = array();
							foreach ($allLinksList as $keyL => $links){

								if(@$allLinksList[$keyL]['type'] == 'events'){
									$event = $allLinksList[$keyL];
									$events[$keyL] = $allLinksList[$keyL];
								
						?>
								<!-- <div class="customBlock customBlockEvent row" style="padding: 20px; border: 1px solid #ddd; margin-bottom: 20px;" data-startDate="<?php //echo $event['startDate']['sec'] ?>">
									<div class="col-xs-12 col-md-5">
										<div class="customBlockImg" style="background-image: url(<?php //echo Yii::app()->createUrl(@$event['profilImageUrl']) ?>);">
										</div>
									</div>
									<div class="col-xs-12 col-md-7">
										<h4 class="date"><?php //echo $event['startDate'] ?> - <?php //echo $event['endDate'] ?></h4>
										<h4><?php //echo $event['name'] ?></h4>
										<p><?php //echo @$event['shortDescription'] ?></p>
									</div>
								</div> -->

						<?php 
								}
							}
						?>
					</div>
					<div id="documents" class="tab-pane">Documents</div>
					<div id="outils" class="tab-pane">Outils</div>
				</div>

			</div>
		</div>
	</div>

	<script type="text/javascript">
		//urlCtrl.loadByHash = false;

		var elt = <?php echo json_encode(@$elt); ?> ;
		var type = <?php echo json_encode(@$type); ?> ;
		var idElt = <?php echo json_encode(@$idElt); ?> ;
		var events = <?php echo json_encode(@$events); ?> ;
		var baseUrl = `<?php echo Yii::app()->createUrl("") ?>` ;
		var defaultBannerUrl = ` <?php echo Yii::app()->theme->baseUrl. '/assets/img/background-onepage/connexion-lines.jpg';?> `;
		

		//var links = <?php echo json_encode(@$linksElt); ?> ;
	</script>

	<?php 

	// CUSTOM JS
	HtmlHelper::registerCssAndScriptsFiles(array('/js/laRaffinerie.js'), $this->module->assetsUrl);
	?>

	<!-- 
		TODO
			Contextualisation agenda et news
			Docs et Outils ( Galerie et Espace Co)
			Inscription projet
			Requete ajax pas acceptée quand user hors ligne
 	-->