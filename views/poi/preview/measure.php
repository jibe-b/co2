<style type="text/css">
	.link-files{
		font-size: 20px;
		text-decoration: none;
	}
	.title-files-list{
		border-bottom: 1px solid rgba(150,150,150, 0.3);
		padding-bottom: 15px;
	}
</style>
<?php 
//var_dump($params);
if(!empty($params["files"])){ ?> 
<h5 class="col-xs-12 title-files-list text-dark"> Voir le récapitulatif de la mesure :</h5>
<?php foreach($params["files"] as $k => $v){ ?>
		<div class='col-xs-12 padding-5 shadow2 margin-top-5 margin-bottom-5'>
			<a href='<?php echo $v["docPath"] ?>' target='_blank' class="link-files"><i class="fa fa-file-pdf-o text-red"></i> <?php echo $v["name"] ?></a>
		</div>
<?php } } ?>
<div id="description"><?php echo (!empty($params["description"]) ? $params["description"] : "" ); ?></div>

<?php
if(!empty($params["level"])){
	echo "<br/>";
	foreach($params["level"] as $k => $v){ ?>
		<div class='col-xs-12 padding-5 shadow2 margin-top-5 margin-bottom-5'>
			<span><h5><?php echo $v["label"] ?> : </h5></span>
			<span><?php echo $v["value"] ?></span>
		</div>
<?php }
}


//$actions = PHDB::find(Action::COLLECTION, array("measures.".(String)$params["_id"] => array('$exists' => 1)));
/*$actions2 = PHDB::aggregate( Action::COLLECTION, array(
						array('$match' => array("measures.".(String)$params["_id"] => array('$exists' => 1))),
						array('$group' => array( '_id' => array('parentId' => '$parentId' , 'parentType' => '$parentType')
						))) );
if(!empty($actions2) && !empty($actions2["result"])){
	$idGroups = array();
	foreach ($actions2["result"] as $key => $value) {
		$idGroups[] = new MongoId($value["_id"]["parentId"]) ;
	}
	$groups = PHDB::find(Organization::COLLECTION, array( "_id" => array('$in' => $idGroups)), array("name"));

	if(!empty($groups)){ ?>
		<br/>
		<h5 class="col-xs-12 title-files-list text-dark"> Liste des groupes utilisants la mesure :</h5>
	<?php
		foreach($groups as $k => $v){ ?>
			<div class='col-xs-3 padding-5 shadow2 margin-top-5 margin-bottom-5'>
				<span><?php echo $v["name"] ?> </span>
			</div>
	<?php }
	}
}*/
?>